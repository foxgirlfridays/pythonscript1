import re

print('Hello! I am theoretically able to use python for basic scripting purposes.')

likesCoffee = input('Hey do you like coffee? (y/n)')
if(likesCoffee=='y'):
    print('Cool!')
    likesCoffee = 'true'
elif(likesCoffee=='yes'):
    print('Cool!')
    likesCoffee = 'true'
elif(likesCoffee=='yep'):
    print('Cool!')
    likesCoffee = 'true'
elif(likesCoffee=='yeah'):
    print('Cool!')
    likesCoffee = 'true'
elif(likesCoffee=='yup'):
    print('Cool!')
    likesCoffee = 'true'
elif(likesCoffee=='n'):
    print('Huh')
    likesCoffee = 'false'
elif(likesCoffee=='no'):
    print('Huh')
    likesCoffee = 'false'
elif(likesCoffee=='na'):
    print('Huh')
    likesCoffee = 'false'
elif(likesCoffee=='nah'):
    print('Huh')
    likesCoffee = 'false'
elif(likesCoffee=='nope'):
    print('Huh')
    likesCoffee = 'false'
else:
    print('Sorry, I did not catch that. Maybe try again?')
    likesCoffee = 0
if(likesCoffee!=0):
    print('Here is some ASCII art of a cup of coffee! Real CompSci culture right here!')
    print('          ')
    print('    \   \ ')
    print('    /   / ')
    print('    \   \ ')
    print('     \   \ ')
    print('|---------------|')
    print('|               |--|')
    print('|               |  |')
    print('|               |--|')
    print(' \             /')
    print('  \___________/')
    print(' ___________________')
    print(' \_________________/ ')

    if(likesCoffee=='true'): 
        print('I like coffee, coffee is good and has a convenient addictive drug called caffeine in it! It makes everything easier!')
    elif(likesCoffee=='false'):
        print('I have horrible taste and do not like coffee unfortunately, and deserve to be locked up for crimes against humanity.')
else:
    exit()

print('I shall figure out the year you were born! How old are you?')
ageOfUser = input()
stringTestOne = re.search('[a-zA-Z]', ageOfUser)
if(stringTestOne==None):
    ageOfUser = int(ageOfUser)
    yearOfBirth = 2021 - ageOfUser
    print('You were born in: ')
    print(yearOfBirth)
    print('Now let\'s do this the opposite way! What year were you born? ')
    yearOfBirthTwo = input()
    stringTestTwo = re.search('[a-zA-Z]', yearOfBirthTwo)
    if(stringTestTwo==None):
        yearOfBirthTwo = int(yearOfBirthTwo)
        ageOfUserTwo = 2021 - yearOfBirthTwo
        print('You are currently: ')
        print(ageOfUserTwo)
        print('years old!')
        print('Cool we figured out whether you like coffee or deserve to be locked up, your year of birth, and your age!')
    else:
        print('Hey you seem to have put a letter into your birth year! Don\'t do that please. Restart the program to continue')
        exit()
else:
    print('Hey you seem to have put a letter into your age! Don\'t do that please. Restart the program to continue')
    exit()