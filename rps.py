# Write your code here :-)
from random import randint

player = input('rock (r), paper (p), or scissors (s)?')
computerPlayNumber = randint(1, 3)
computerPlayName = 'n'

if(player == 'r'):
    chosenPlay = 'Rock'
elif(player == 'p'):
    chosenPlay = 'Paper'
elif(player == 's'):
    chosenPlay = 'Scissors'
else:
    print('You seem to have had a misinput. Only type r p or s!')
    exit(1)

if(computerPlayNumber == 1):
    computerPlayName = 'Rock'
elif(computerPlayNumber == 2):
    computerPlayName = 'Paper'
elif(computerPlayNumber == 3):
    computerPlayName = 'Scissors'

print(chosenPlay, ' vs ')
print(computerPlayName)

fancyPlay = input('bash (b), throw (t), nothing (n)')
if(fancyPlay == 'b'):
    if(chosenPlay == 'Rock'):
        print('You bash your opponent with a rock. It is quite effective.')
        usedRock = 'true'
    else:
        print('Bashing your opponent with an object weighing nearly nothing does not do very much. You get bashed yourself.')
        usedRock = 'false'
elif(fancyPlay == 't'):
    if(chosenPlay == 'Rock'):
        print('You throw a rock at your opponent. It works pretty well, and knocks them out.')
        usedRock = 'true'
    else:
        print('Your object is not very aerodynamic. It misses and you get bashed.')
        usedRock = 'false'
elif(fancyPlay == 'n'):
    print('As it is your turn, nothing happens for the rest of eternity.')
    usedRock = 'peace'
else:
    print('You seem to have had a misinput. Only type b t or n!')
    exit(2)

if(usedRock == 'true'):
    print('Congratulations, you actually won a game of rock paper scissors arena.')
elif(usedRock == 'false'):
    print('Oh no! You lost.')
else:
    print('So, the weather huh.')